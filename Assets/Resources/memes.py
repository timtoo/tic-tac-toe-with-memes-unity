#!/usr/bin/python

# Output the JSON meme database (to ensure well formed)

import json

memes = [
    {
        "name": "Lame Pun Coon",
        "object": "racoon",
        "kym_url": "http://knowyourmeme.com/memes/lame-pun-coon",
    },

    {
        "name": "Confession Bear",
        "object": "bear",
        "kym_url": "http://knowyourmeme.com/memes/confession-bear",
    },

    {
        "name": "Doge",
        "object": "dog",
        "kym_url": "http://knowyourmeme.com/memes/doge",
    },

    {
        "name": "Dramatic Gopher",
        "object": "gopher",
        "kym_url": "http://knowyourmeme.com/memes/dramatic-chipmunk",
    },

    {
        "name": "Grumpy Cat",
        "object": "cat",
        "kym_url": "http://knowyourmeme.com/memes/grumpy-cat",
    },

    {
        "name": "Insanity Wolf",
        "object": "wolf",
        "kym_url": "http://knowyourmeme.com/memes/insanity-wolf",
    },

    {
        "name": "Overly Attached Girl",
        "object": "woman",
        "kym_url": "http://knowyourmeme.com/memes/overly-attached-girlfriend",
    },

    {
        "name": "Awkward Moment Seal",
        "object": "seal",
        "kym_url": "http://knowyourmeme.com/memes/awkward-moment-seal",
    },

]

print (json.dumps(memes, indent=4));

