﻿using UnityEngine;
using System.Collections;

public class Global
{
	public static string[] MemeName = {
		"Bad Pun Coon",
		"Confession Bear",
		"Doge",
		"Dramatic Gopher",
		"Grumpy Cat",
		"Insanity Wolf",
		"Overly Attached Girl",
		"Awkward Moment Seal",
	};

	public static int scene_width = 1680;
	public static int scene_hight = 1050;
	public static int pixels_per_unit = 100;

	public static float camera_size = (float)scene_width / ((((float)scene_width / scene_hight) * 2) * pixels_per_unit);


}
