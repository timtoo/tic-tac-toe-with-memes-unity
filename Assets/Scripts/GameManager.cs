﻿using UnityEngine;
using System.Collections;


public class PlayerObject
{

	public GameObject avatar;
	public int playerType;

}


public class GameManager : MonoBehaviour
{

	public static GameManager instance = null;

	public int[] player = { -1, -1 };
	public int[] grid;

	public int playerTurn = 0;
	public int turnCount = 0;
	public int undoCount = 0;
	public int state = 0;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);    

		DontDestroyOnLoad (gameObject);

		//InitGame();
	}

	public static GameManager Instance {
		get {
			if (instance == null) {
				instance = new GameManager ();
				DontDestroyOnLoad (instance);
			}
			return instance;
		}
	}
		
		
}
