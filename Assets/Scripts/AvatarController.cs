using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AvatarController : MonoBehaviour
{
	public Sprite[] avatars;

	private int _index = 0;
	SpriteRenderer spriteRenderer;

	public int index {
		get { return _index; }
		set { 
			Debug.Log (string.Format ("Avatar value: {0}  (Length: {1})", value.ToString (), avatars.Length));
			_index = value; 
			spriteRenderer.sprite = avatars [_index];  
		}
	}

	// Use this for initialization
	void Awake ()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Space)) {
			Debug.Log ("space from " + gameObject.GetInstanceID ());
		}


	}

	void OnMouseDown ()
	{
		Debug.Log ("mousedown from " + gameObject.GetInstanceID ());
		Canvas c = gameObject.GetComponentInChildren<Canvas> ();
		//c.enabled = !c.enabled;
	}

	public void NextAvatar ()
	{
		Debug.Log ("Next Avatar after " + index.ToString ());
		if (index < avatars.Length - 1) {
			index += 1;
		} else {
			index = 0;
		}
	}


}
