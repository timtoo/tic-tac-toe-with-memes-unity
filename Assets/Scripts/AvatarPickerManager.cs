﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AvatarPickerManager : MonoBehaviour
{

	public GameObject avatarPrefab;
	public GameObject avatarButtonPrefab;

	public int gap = 60;
	public int columns = 4;

	Queue LotsOfAvatars;

	// Use this for initialization
	void Start ()
	{

		LotsOfAvatars = new Queue (500);

		Camera.main.aspect = 16f / 10f;

		//StartCoroutine ("RandomAvatar");

		DrawGrid ();

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}


	void DrawGrid ()
	{
		float xx;
		float yy;
		Vector3 vv;
		GameObject gg;

		float image_width = 256;
		float offset_y = 350;
		float offset_x = (float)(Global.scene_width - (((image_width + gap) * columns) - gap)) / 2 + (image_width / 2);

		for (int i=0; i < Global.MemeName.Length; i++) {
			xx = offset_x + ((i % columns) * (image_width + gap));
			yy = offset_y + ((i / columns) * (image_width + gap));

			vv = new Vector3 (xx / 100, yy / -100, -1);

			gg = Instantiate (avatarButtonPrefab, vv, Quaternion.identity) as GameObject;
			gg.name = i.ToString ();
			AvatarController ac = gg.GetComponent<AvatarController> ();
			ac.index = i;

			AvatarButtonController abc = gg.GetComponent<AvatarButtonController> ();
			abc.footer.text = Global.MemeName [i];
			abc.header.text = gg.name;

			Debug.Log ("instantiate " + i.ToString ());

		}





	}


	GameObject InstantiateAvatar (float x, float y, int index)
	{
		GameObject g = Instantiate (avatarPrefab, new Vector3 (x, -y, -1), Quaternion.identity) as GameObject;
		AvatarController ac = g.GetComponent<AvatarController> ();
		ac.index = index;
		return g;
	}

	IEnumerator  RandomAvatar ()
	{
		for (int a=0; a < 1000000; a++) {
			GameObject g = InstantiateAvatar (Random.Range (0, 2f * Camera.main.orthographicSize * Camera.main.aspect), 
		                   Random.Range (0, 2f * Camera.main.orthographicSize), 
		                   Random.Range (0, 7)) as GameObject;
			LotsOfAvatars.Enqueue (g);

			if (LotsOfAvatars.Count >= 499) {
				g = LotsOfAvatars.Dequeue () as GameObject;
				DestroyObject (g);
			}

			yield return new WaitForSeconds (.1f);
		}

	}

	public void Quit ()
	{
		Application.Quit ();
	}

}

/* 

var image_width = sprite_get_width(sAvatar);
var gap = 50;
var columns = 4
var offset_y = 250
var offset_x = (room_width - (((image_width + gap) * columns) - gap)) div 2

*/